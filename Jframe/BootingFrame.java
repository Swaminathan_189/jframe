import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SpringLayout;
import javax.swing.border.Border;


public class BootingFrame extends javax.swing.JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8845838556321698551L;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		Border blackLine = BorderFactory.createLineBorder(Color.black);
		
		JFrame jFrame = new JFrame("MR Automation Tool");
		JLabel bugIdLabel = new JLabel("Bug Id: ");
		bugIdLabel.setBounds(50, 10, 70, 22);
		bugIdLabel.setFont(new Font("Arial", Font.BOLD,16));
		
		JTextField bugIdText = new JTextField(30);
		bugIdLabel.setLabelFor(bugIdText);
		bugIdText.setBounds(180, 10, 120, 22);
		
		JLabel sourceBranchLabel = new JLabel("Source Branch: ");
		sourceBranchLabel.setBounds(50, 60, 130, 22);
		sourceBranchLabel.setFont(new Font("Arial", Font.BOLD,16));
		
		JTextField sourceBranchText = new JTextField(30);
		sourceBranchLabel.setLabelFor(sourceBranchText);
		sourceBranchText.setBounds(180, 60, 120, 22);
		
		JLabel assigneeLabel = new JLabel("Assignee: ");
		assigneeLabel.setBounds(50, 110, 90, 22);
		assigneeLabel.setFont(new Font("Arial", Font.BOLD,16));
		
		JComboBox<String> assigneeComboBox = new JComboBox<String>();
		assigneeComboBox.addItem("Sivatharun");
		assigneeComboBox.addItem("Hariharan");
		assigneeComboBox.addItem("Harikrishnan");
		assigneeComboBox.setBounds(180, 110, 120, 22);
		
		JLabel ptfCheckBoxLabel = new JLabel("PTF: ");
		ptfCheckBoxLabel.setBounds(100, 160, 50, 22);
		ptfCheckBoxLabel.setFont(new Font("Arial", Font.BOLD,16));
		
		JCheckBox ptfCheckBox = new JCheckBox();
		ptfCheckBoxLabel.setLabelFor(ptfCheckBox);
		ptfCheckBox.setBounds(135, 160, 20, 22);
		
		JLabel qcCheckBoxLabel = new JLabel("QC: ");
		qcCheckBoxLabel.setBounds(190, 160, 50, 22);
		qcCheckBoxLabel.setFont(new Font("Arial", Font.BOLD,16));
		
		JCheckBox qcCheckBox = new JCheckBox();
		qcCheckBoxLabel.setLabelFor(qcCheckBox);
		qcCheckBox.setBounds(218, 160, 50, 22);
		
		JButton createButton = new JButton("Create MR");
		createButton.setBounds(50, 210, 115, 25);
		
		JButton resetButton = new JButton("Reset");
		resetButton.setBounds(185, 210, 115, 25);

		JPanel jPanel = new JPanel();
		jPanel.setLayout(new SpringLayout());
		jPanel.add(bugIdLabel);
		jPanel.add(bugIdText);
		jPanel.add(sourceBranchLabel);
		jPanel.add(sourceBranchText);
		jPanel.add(assigneeLabel);
		jPanel.add(assigneeComboBox);
		jPanel.add(ptfCheckBoxLabel);
		jPanel.add(ptfCheckBox);
		jPanel.add(qcCheckBoxLabel);
		jPanel.add(qcCheckBox);
		jPanel.add(createButton);
		jPanel.add(resetButton);
		
		jPanel.setBorder(blackLine);
		jPanel.setBounds(1, 2, 380, 350);
		jPanel.setLayout(null);
		jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jFrame.setBounds(780,300,400, 400);
		jFrame.setBackground(Color.green);
		jFrame.getContentPane().add(jPanel);
		jFrame.setLayout(null);
		jFrame.setVisible(true);
		
	}
}
